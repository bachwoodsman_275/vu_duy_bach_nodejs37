import express from "express";
import {
  addRating,
  getLikeRes,
  getListRating,
  getListRes,
  userOrder,
} from "../controller/resController.js";

export const resRoute = express.Router();

resRoute.get("/get-res", getListRes);
resRoute.get("/get-like-res/:resId", getLikeRes);
resRoute.get("/get-list-rating-res/:resId", getListRating);
resRoute.post("/add-rating", addRating);
resRoute.post("/user-order", userOrder);
