import express from "express";
import { login, refToken, signUp } from "../controller/authController.js";

export const authRoute = express.Router();

authRoute.post("/sign-up", signUp);
authRoute.post("/login", login);
authRoute.post("/ref-token", refToken);
