import express from "express";
import cors from "cors";
import { rootRoute } from "./router/rootRoute.js";

const app = express();

app.use(cors());
app.use(express.json());
app.listen(8080, () => {
  console.log("MY SEVER IS RUNNING AT", 8080);
});
app.use(rootRoute);
