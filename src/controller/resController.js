import sequelize from "../config/connect_db.js";
import { decodeToken } from "../config/jwt.js";
import { responseData } from "../config/response.js";
import initModels from "../model/init-models.js";

const model = initModels(sequelize);

export const getListRes = async (req, res) => {
  let data = await model.restaurants.findAll();
  responseData(res, "Get list restaurants successful", data, 200);
};
export const getLikeRes = async (req, res) => {
  let { resId } = req.params;

  let data = await model.like_res.findAll({
    where: {
      res_id: resId,
    },
    include: ["user"],
  });
  responseData(res, "Get list likes res successful", data, 200);
};

// get list rating res
export const getListRating = async (req, res) => {
  let { resId } = req.params;

  let data = await model.rate_res.findAll({
    where: {
      res_id: resId,
    },
    include: ["user"],
  });
  responseData(res, "Get list rating res successful", data, 200);
};
//add rating
export const addRating = async (req, res) => {
  let { token } = req.headers;
  let { amount, res_id } = req.body;
  let dateRate = new Date().getTime();
  let dToken = decodeToken(token);
  let getUser = await model.users.findOne({
    where: { user_id: dToken.data.user_id },
  });

  // check user
  if (!getUser) {
    responseData(res, "Login Please", "", 401);
    return;
  }
  // add amount
  let newRating = await model.rate_res.create({
    user_id: getUser.dataValues.user_id,
    res_id,
    amount,
    date_rate: dateRate,
  });
  responseData(res, "Add Rating Successful", newRating, 200);
};

// user order
export const userOrder = async (req, res) => {
  let { token } = req.headers;
  let dToken = decodeToken(token);
  let { food_id, amount, code } = req.body;
  let getUser = await model.users.findOne({
    where: {
      user_id: dToken.data.user_id,
    },
  });
  let newOrder = await model.orders.create({
    user_id: getUser.dataValues.user_id,
    food_id,
    amount,
    code,
    arr_sub_id: "",
  });

  responseData(res, " Successful", newOrder, 200);
};
