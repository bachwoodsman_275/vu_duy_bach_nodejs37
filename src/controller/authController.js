import sequelize from "../config/connect_db.js";
import {
  checkRefToken,
  checkToken,
  createRefToken,
  createToken,
  decodeToken,
} from "../config/jwt.js";
import { responseData } from "../config/response.js";
import initModels from "../model/init-models.js";
import bcrypt from "bcrypt";
const model = initModels(sequelize);

export const login = async (req, res) => {
  let { email, password } = req.body;
  let key = new Date().getTime();
  let getUserThroughEmail = await model.users.findOne({
    where: {
      email: email,
    },
  });

  if (getUserThroughEmail) {
    if (
      bcrypt.compareSync(
        password.toString(),
        getUserThroughEmail.dataValues.password
      )
    ) {
      let token = createToken({
        user_id: getUserThroughEmail.dataValues.user_id,
        key,
      });
      let refToken = createRefToken({
        user_id: getUserThroughEmail.dataValues.user_id,
        key,
      });
      // update refToken into database
      await model.users.update(
        {
          ...getUserThroughEmail,
          refresh_token: refToken,
        },
        { where: { user_id: getUserThroughEmail.dataValues.user_id } }
      );
      responseData(res, "Login Successful", { token, refToken }, 200);
    } else {
      responseData(res, "Password is not valid", "", 404);
    }
  } else {
    responseData(res, "Email is not valid", "", 404);
  }
};

export const signUp = async (req, res) => {
  let { full_name, email, password } = req.body;

  let checkUserEmail = await model.users.findOne({
    where: {
      email: email,
    },
  });
  if (checkUserEmail) {
    responseData(res, "Email is already registerd", "", 404);
    return;
  }
  let data = await model.users.create({
    full_name: full_name,
    email: email,
    password: bcrypt.hashSync(password.toString(), 10),
    refresh_token: "",
  });
  responseData(res, "sign up successful", data, 200);
};

// refToken
export const refToken = async (req, res) => {
  let { token } = req.headers;
  let dToken = decodeToken(token);
  let getUser = await model.users.findOne({
    where: { user_id: dToken.data.user_id },
  });

  // check token
  let check = checkToken(token);

  if (check != null && check.name != "TokenExpiredError") {
    responseData(res, "Token", check.name, 401);
    return;
  }
  // check refresh token
  let checkRef = checkRefToken(getUser.dataValues.refresh_token);
  if (checkRef != null) {
    responseData(res, "Refresh Token", check.name, 401);
    return;
  }
  let dRefToken = decodeToken(getUser.dataValues.refresh_token);
  if (dToken.data.key != dRefToken.data.key) {
    responseData(res, "Error", "", 401);
    return;
  }
  // create new token
  let newToken = createToken({ user_id: getUser.dataValues.user_id });
  responseData(res, "Create new token successful", newToken, 200);
};
