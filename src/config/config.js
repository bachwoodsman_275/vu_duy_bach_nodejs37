import dotenv from "dotenv";

dotenv.config();

export const config = {
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASS,
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  dialect: process.env.DB_DIALECT,
};
