import jwt from "jsonwebtoken";

export const createToken = (data) => {
  let token = jwt.sign({ data }, "BIMAT", {
    algorithm: "HS256",
    expiresIn: "10m",
  });
  return token;
};
export const createRefToken = (data) => {
  let refToken = jwt.sign({ data }, "KOBIMAT", {
    algorithm: "HS256",
    expiresIn: "1d",
  });
  return refToken;
};
export const decodeToken = (token) => {
  return jwt.decode(token);
};
export const checkToken = (token) => {
  return jwt.verify(token, "BIMAT", (error, decoded) => {
    return error;
  });
};
export const checkRefToken = (token) => {
  return jwt.verify(token, "KOBIMAT", (error, decoded) => {
    return error;
  });
};
export const verifyToken = (req, res, next) => {
  let check = checkToken(token);
};
