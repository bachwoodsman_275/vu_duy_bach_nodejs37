import { Sequelize } from "sequelize";
import { config } from "./config.js";

const sequelize = new Sequelize(config.database, config.user, config.password, {
  port: config.port,
  host: config.host,
  dialect: config.dialect,
});

// try {
//   await sequelize.authenticate();
//   console.log("MY CONNECT DB IS OK ");
// } catch (error) {
//   console.log(error);
// }

export default sequelize;
